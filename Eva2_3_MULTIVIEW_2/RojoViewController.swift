//
//  RojoViewController.swift
//  Eva2_3_MULTIVIEW_2
//
//  Created by TEMPORAL2 on 14/10/16.
//  Copyright © 2016 TEMPORAL2. All rights reserved.
//

import UIKit

class RojoViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    let data = ["lalo", "alan", "majo", "ana", "diego", "edgar","frank", "javi", "mabel", "duran", "erick", "andres", "saenz"]
    
    @IBOutlet weak var pvDatos: UIPickerView!
    
    @IBAction func onClickViewData(sender: AnyObject) {
        let row = pvDatos.selectedRowInComponent(0)
        
        let selectRow = data[row]
        
        let aler = UIAlertController(title: "Seleccion de datos",message: "Seleccionaste: \(selectRow)", preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Destructive, handler: nil)
        aler.addAction(action)
        presentViewController(aler, animated: true, completion: nil)
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
