//
//  NegroViewController.swift
//  Eva2_3_MULTIVIEW_2
//
//  Created by TEMPORAL2 on 14/10/16.
//  Copyright © 2016 TEMPORAL2. All rights reserved.
//

import UIKit

class NegroViewController: UIViewController {

    
    @IBOutlet weak var dpDate: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let fecha  = NSDate()
        dpDate.setDate(fecha, animated: false)
        // Do any additional setup after loading the view.
    }
    
    
    

    @IBAction func onClickDataPicker(sender: AnyObject) {
        let fecha = dpDate.date
        let alerta = UIAlertController(title: "Fecha seleccionada",
            message: "la fecha es \(fecha)",
        preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "ok", style: .Destructive, handler: nil)
        
        alerta.addAction(action)
        
        presentViewController(alerta, animated: true, completion: nil)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
